# IDS-721 minilab10

This project demonstrates a serverless architecture using AWS Lambda and API Gateway to provide for **sentence similarity evaluation**. The backend is powered by a Rust application, which is containerized using Docker and deployed to AWS Lambda. The endpoint allows clients to send sentences and compare their similarity to a source sentence using a transformer model from Hugging Face.


## Usage

To use the endpoint, you can send a POST request with a JSON body containing the source sentence and the sentences to compare, you can use my api site as a example(https://2vztqiizg3.execute-api.us-east-1.amazonaws.com/default/mini10l):

```bash
curl -X POST https://2vztqiizg3.execute-api.us-east-1.amazonaws.com/default/mini10l \
  -d '{"source_sentence": "That is a happy person", "sentences": ["That is a happy dog", "That is a very happy person"]}' \
  -H "Content-Type: application/json"
```
result looks like this:
```
[0.6945773363113403, 0.9429150819778442]
```

![](./1.png)

## my deployment
![](./2.png)
![](./3.png)


## Features

- **Dockerized Rust application**: Uses Docker to package the Rust application for deployment.
- **Serverless deployment on AWS Lambda**: Leverages AWS Lambda for running the service without managing servers.
- **API Gateway Integration**: Utilizes AWS API Gateway to expose the Rust application as a RESTful API.

## Requirements

- AWS CLI configured with Administrator access
- Docker installed on your machine
- Rust and Cargo installed locally
- Access to AWS ECR and AWS Lambda

## Getting Started

### Setup

1. **Clone the repository**:
    ```bash
    git clone https://gitlab.com/Oliver_Wang_atDuke/minilab10.git
    cd your-repository-folder
    ```

2. **Build the Docker image**:
    ```bash
    docker build -t rust-lambda .
    ```

3. **Push the Docker image to AWS ECR**:
    Follow AWS documentation to create a repository if not already done, tag your Docker image accordingly, and push it to ECR.

4. **Deploy to AWS Lambda**:
    - Create a new Lambda function.
    - Choose the option to use a container image.
    - Select the image you've pushed to ECR.

5. **Set up API Gateway**:
    - Create a new API Gateway.
    - Configure a new POST method linked to your Lambda function.
    - Deploy the API to a new stage.


