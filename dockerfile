FROM rust:latest as builder

WORKDIR /usr/src/myapp

RUN cargo install cargo-lambda

COPY . .

RUN cargo lambda build --release

FROM public.ecr.aws/lambda/provided:al2

COPY --from=builder /usr/src/myapp/target/release/mini10l /var/task/

CMD [ "mini10l" ]
