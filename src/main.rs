use lambda_http::{run, service_fn, Body, Error, Request, Response, IntoResponse};
use serde::{Serialize, Deserialize};
use serde_json::{Value, json};

#[derive(Serialize, Deserialize)]
struct Input {
    source_sentence: String,
    sentences: Vec<String>,
}

#[derive(Serialize, Deserialize)]
struct ApiRequest {
    inputs: Input,
}




async fn function_handler(event: Request) -> Result<impl IntoResponse, Error> {
    let input: Input = serde_json::from_slice(event.body().as_ref())?;

    
    let api_request = ApiRequest { inputs: input };

    
    let client = reqwest::Client::new();
    let response = client.post("https://api-inference.huggingface.co/models/sentence-transformers/all-MiniLM-L6-v2")
        .json(&api_request)
        .header("Content-Type", "application/json")
        .header("Authorization", "Bearer XXXXXXXXXXXXXX") // replace with your own hugging face api
        .send()
        .await?;

    
    if response.status().is_success() {
        let body = response.text().await?;
        Ok(Response::builder()
            .status(200)
            .header("Content-Type", "application/json")
            .body(Body::from(body))?)
    } else {
        let error_message = format!("Error: {}", response.status());
        Ok(Response::builder()
            .status(400)
            .body(error_message.into())?)
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt::init();
    run(service_fn(function_handler)).await
}
